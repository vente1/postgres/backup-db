# Backup DB
Backup DB is a docker for create database backup (PostgreSQL)
and copy it to backup folder. Also sends metrics a Prometheus through
PushGateWay.

![alt schema-db](images/schema-backup-db.png)

# Variables
|          Name         	|          Description          	|      Default      	|
|---------------------------|-----------------------------------|----------------------	|
| POSTGRES_USER         	| Database username             	| postgres          	|
| POSTGRES_PASSWORD     	| Database password             	| password          	|
| POSTGRES_HOSTNAME     	| Database hostname             	| postgresql        	|
| POSTGRES_DUMP_PATH    	| Temporal path                 	| /tmp/backup       	|
| BACKUP_FOLDER             | Path for saving backups           | /backups              |
| PUSHGATEWAY_HOST      	| PushGateWay hostname          	| pushgateway:9091  	|
| PUSHGATEWAY_JOB       	| PushGateWay job name          	| POSTGRES_HOSTNAME 	|
| GZIP                      | Compression ratio                 | -9                    |

# Metrics
|                           Name                          |                        Description                       	   |
|---------------------------------------------------------|----------------------------------------------------------------|
| backup_db{label="vente"}                                | Outcome of the backup database job (0=failed, 1=success) 	   |
| backup_duration_seconds{label="vente",stage="dump"}     | Duration of create dump execution in seconds             	   |
| backup_duration_seconds{label="vente",stage="compress"} | Duration of compress dump execution in seconds           	   |
| backup_duration_seconds{label="vente",stage="copy"}     | Duration of copy backup to backup folder execution in seconds  |
| backup_size_bytes{label="vente"}                        | Duration of the script execution in seconds              	   |
| backup_created_date_seconds{label="vente"}              | Created date in seconds                                  	   |