FROM alpine:3.10

LABEL maintainer="nacho@gesplan.es"

ENV POSTGRES_PORT="5432" \
	POSTGRES_HOSTNAME="postgresql" \
	POSTGRES_USER="postgres" \
	POSTGRES_PASSWORD="password" \
	POSTGRES_PASS_FILE='/root/.pgpass' \
	POSTGRES_DUMP_PATH="/tmp/backup" \
	GZIP="-9"

COPY scripts /

RUN apk add --no-cache \
		curl \
		postgresql-client \
		bash && \
	rm -rf /var/cache/apk/*

ENTRYPOINT ["/docker-entrypoint.sh"]
